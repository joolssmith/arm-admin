set pth=D:\Git\arm-admin\flexnet_client-xt-java-x64_windows-2019.11.0

call mvn install:install-file ^
	-Dfile=%pth%/lib/flxBinary.jar ^
	-DgroupId=com.flexera.fne ^
	-DartifactId=binary ^
	-Dversion=19.11.0 ^
	-Dpackaging=jar
	
call mvn install:install-file ^
	-Dfile=%pth%/lib/flxClient.jar ^
	-DgroupId=com.flexera.fne ^
	-DartifactId=client ^
	-Dversion=19.11.0 ^
	-Dpackaging=jar
	
call mvn install:install-file ^
	-Dfile=%pth%/lib/flxClientNative.jar ^
	-DgroupId=com.flexera.fne ^
	-DartifactId=client.native ^
	-Dversion=19.11.0 ^
	-Dpackaging=jar

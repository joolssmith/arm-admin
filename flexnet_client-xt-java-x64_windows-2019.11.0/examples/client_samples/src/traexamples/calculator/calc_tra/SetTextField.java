package calc_tra;
/**
 * Copyright (c) 2011-2019 Flexera Software LLC. All Rights Reserved. This software has been
 * provided pursuant to a License Agreement containing restrictions on its use. This software
 * contains valuable trade secrets and proprietary information of Flexera Software LLC and is
 * protected by law. It may not be copied or distributed in any form or medium, disclosed to third
 * parties, reverse engineered or used in any manner not provided for in said License Agreement
 * except with the prior written authorization from Flexera Software LLC.
 */
 
import org.luaj.vm2.Varargs;
import com.flexnet.tra.VarArgFunction;
import org.luaj.vm2.LuaValue;
import javax.swing.JTextField;

public class SetTextField extends VarArgFunction {
	public Varargs invoke(Varargs v){
		JTextField result = (JTextField)v.arg(1).checkuserdata();
		String s = v.arg(2).checkjstring();
		result.setText(s);
		return LuaValue.NIL;
	}
}

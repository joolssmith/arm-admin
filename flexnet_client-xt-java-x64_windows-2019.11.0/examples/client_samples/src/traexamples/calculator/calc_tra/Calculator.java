/**
 * Copyright (c) 2011-2019 Flexera Software LLC. All Rights Reserved. This software has been
 * provided pursuant to a License Agreement containing restrictions on its use. This software
 * contains valuable trade secrets and proprietary information of Flexera Software LLC and is
 * protected by law. It may not be copied or distributed in any form or medium, disclosed to third
 * parties, reverse engineered or used in any manner not provided for in said License Agreement
 * except with the prior written authorization from Flexera Software LLC.
 */
package calc_tra;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.Logger;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.tra.CoerceJavaToLua;
import com.flexnet.tra.Calculator_tra;
import com.flexnet.tra.Calculator_tdt_TDT;


public class Calculator extends JFrame {

  // This example(using TRA) program allows you to:
  // 1. Acquire feature "highres" license from license source read from a file .
  // 2. Invokes a simple calculator application

  private static final long serialVersionUID = 1L;
  static Logger logger = Logger.getLogger(Calculator.class);
  static String lastCommand = new String();
  double res = 0;
  JPanel panel = new JPanel();
  JTextField result = new JTextField("0", 45);
  private boolean start;
  private static String licenseFile;
  private Calculator_tra tra;
  private Calculator_tdt_TDT[] tdt;


  public Calculator(Calculator_tra t) {
	tra = t;
	setLayout(new BorderLayout());
	result.setEditable(false);
	add(result, BorderLayout.NORTH);
	panel.setLayout(new GridLayout(4, 4));
	start = true;
	tdt =
		new Calculator_tdt_TDT[] {
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_ZERO_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_ONE_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_TWO_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_THREE_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_FOUR_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_FIVE_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_SIX_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_SEVEN_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_EIGHT_ALIAS_1),
			new Calculator_tdt_TDT(tra, Calculator_tra.TRA_VARIABLE_NINE_ALIAS_1)};
	ActionListener insert = new InsertAction();
	ActionListener command = new CommandAction();

	addButton(tra.get_string(Calculator_tra.TRA_STRING_SEVEN_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_EIGHT_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_NINE_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_DIVIDE_ALIAS_1), command);

	addButton(tra.get_string(Calculator_tra.TRA_STRING_FOUR_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_FIVE_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_SIX_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_MULTIPLY_ALIAS_1), command);

	addButton(tra.get_string(Calculator_tra.TRA_STRING_ONE_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_TWO_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_THREE_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_MINUS_ALIAS_1), command);

	addButton(tra.get_string(Calculator_tra.TRA_STRING_ZERO_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_PERIOD_ALIAS_1), insert);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_EQUALS_ALIAS_1), command);
	addButton(tra.get_string(Calculator_tra.TRA_STRING_PLUS_ALIAS_1), command);
	add(panel, BorderLayout.CENTER);

	tra.call(Calculator_tra.TRA_FUNCTION_INIT_TEXTFIELD_ALIAS_1, 0, CoerceJavaToLua.coerce(result));
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  /**
   * This action inserts the button action string to the end of the display text.
   */
  private class InsertAction implements ActionListener {
	public void actionPerformed(ActionEvent event) {
	  String input = event.getActionCommand();
	  if (start) {
		tra.call(Calculator_tra.TRA_FUNCTION_SET_RESULT_ALIAS_1,
			Calculator_tra.TRA_STRING_RESULT_ALIAS_2, CoerceJavaToLua.coerce(""));
		start = false;
	  }
	  tra.call(Calculator_tra.TRA_FUNCTION_SET_RESULT_ALIAS_1,
		  Calculator_tra.TRA_STRING_RESULT_ALIAS_2,
		  CoerceJavaToLua.coerce(tra.get_string(Calculator_tra.TRA_STRING_RESULT_ALIAS_3) + input));
	}
  }

  /**
   * This action executes the command that the button action string denotes.
   */
  private class CommandAction implements ActionListener {
	public void actionPerformed(ActionEvent event) {
	  String command = event.getActionCommand();
	  if (start) {
		lastCommand = command;
	  } else {
		calculate(Double.parseDouble(tra.get_string(Calculator_tra.TRA_STRING_RESULT_ALIAS_3)));
		lastCommand = command;
		start = true;
	  }
	}
  }

  /**
   * Adds a button to the center panel.
   *
   * @param label the button label
   * @param listener the button listener
   */
  private void addButton(String label, ActionListener listener) {
	JButton button = new JButton(label);
	button.addActionListener(listener);
	panel.add(button);
  }

  /**
   * Carries out the pending calculation.
   *
   * @param x the value to be accumulated with the prior result.
   */
  public void calculate(double x) {
	if (!lastCommand.isEmpty()) {
	  logger.debug(tra.get_string(Calculator_tra.TRA_STRING_ENTERED_INPUT_ALIAS_1) + res + tra.get_string(Calculator_tra.TRA_STRING_SPACE_ALIAS_2) + lastCommand + tra.get_string(Calculator_tra.TRA_STRING_SPACE_ALIAS_1) + x);
	}

	if (lastCommand.equals(tra.get_string(Calculator_tra.TRA_STRING_PLUS_ALIAS_2)))
	  res += x;
	else if (lastCommand.equals(tra.get_string(Calculator_tra.TRA_STRING_MINUS_ALIAS_2)))
	  res -= x;
	else if (lastCommand.equals(tra.get_string(Calculator_tra.TRA_STRING_MULTIPLY_ALIAS_2))) {
	  if (tra.get_value(Calculator_tra.TRA_VARIABLE_LICENSED_ALIAS_5) > 1) {
		res *= x;
	  } else {
		JOptionPane.showMessageDialog(this, tra.get_string(Calculator_tra.TRA_STRING_MULTIPLY_NOT_LICENSED_ALIAS_1), tra.get_string(Calculator_tra.TRA_STRING_ALERT_ALIAS_2),
			JOptionPane.ERROR_MESSAGE);
	  }
	} else if (lastCommand.equals(tra.get_string(Calculator_tra.TRA_STRING_DIVIDE_ALIAS_2))) {
	  if (tra.get_value(Calculator_tra.TRA_VARIABLE_LICENSED_ALIAS_4) > 1) {
		res /= x;
	  } else {
		JOptionPane.showMessageDialog(this, tra.get_string(Calculator_tra.TRA_STRING_DIVIDE_NOT_LICENSED_ALIAS_1), tra.get_string(Calculator_tra.TRA_STRING_ALERT_ALIAS_1),
			JOptionPane.ERROR_MESSAGE);
	  }
	} else if (lastCommand.equals(tra.get_string(Calculator_tra.TRA_STRING_EQUALS_ALIAS_2)) || lastCommand.isEmpty()) {
	  res = x;
	}
	tra.call(Calculator_tra.TRA_FUNCTION_SET_RESULT_ALIAS_3,
		Calculator_tra.TRA_STRING_RESULT_ALIAS_2, CoerceJavaToLua.coerce("" + res));

	if (!lastCommand.isEmpty()) {
	  logger.debug(tra.get_string(Calculator_tra.TRA_STRING_RESULT_ALIAS_1) + res);
	}
  }



  public static void main(String[] args) {


	final Calculator_tra tra = new Calculator_tra();

	// 	System.out.println("LICENSED = " + tra.get_value(Calculator_tra.TRA_VARIABLE_LICENSED_ALIAS_3));
	// Validate command-line arguments
	if (!validateArgs(args, tra)) {
	  return;
	}
	if (flxexamples.IdentityClient.IDENTITY_DATA == null) {
	  System.out.println(tra.get_string(Calculator_tra.TRA_STRING_NO_IDENTITY_DATA_ALIAS_1));
	  return;
	}
	EventQueue.invokeLater(new Runnable() {
	  public void run() {

		Calculator calc = new Calculator(tra);
		calc.setTitle("Calculator_TRA");
		calc.setSize(241, 217);
		calc.setLocation(400, 250);
		calc.setVisible(true);
		ILicensing licensing = null;

		try {

		  // Initialize ILicensing interface with identity data using file-based trusted storage in
		  // user's home directory and hard-coded string hostid "1234567890"
		  licensing =
			  LicensingFactory.getLicensing(flxexamples.IdentityClient.IDENTITY_DATA,
				  System.getProperty(tra.get_string(Calculator_tra.TRA_STRING_USER_HOME_ALIAS_1)),
				  tra.get_string(Calculator_tra.TRA_STRING_HOST_ID_ALIAS_1),
				  tra.get_string(Calculator_tra.TRA_STRING_BASIC_CLIENT_ALIAS_1));
		  // Get ILicenseManager interface, the primary object for licensing-related functionality
		  ILicenseManager licenseManager = licensing.getLicenseManager();
		  // Add buffer license source
		  System.out.println("Reading data from " + licenseFile + ".");
		  licenseManager.addBufferLicenseSource(BinaryMessage.readData(licenseFile));

		  CalculatorUserData user_data =
			  new CalculatorUserData(licensing, licenseManager, tra,
				  Calculator_tra.TRA_STRING_HIGHRES_ALIAS_1,
				  Calculator_tra.TRA_STRING_VERSION_ALIAS_1,
				  Calculator_tra.TRA_VARIABLE_LICENSED_ALIAS_1,
				  Calculator_tra.TRA_VARIABLE_LICENSED_ALIAS_2);

		  // Acquire 1 "highres" license
		  tra.iff(Calculator_tra.TRA_SNIF_ACQUIRE_ALIAS_2, CoerceJavaToLua.coerce(user_data));

		} catch (FlxException e) {
		  System.out.println(tra.get_string(Calculator_tra.TRA_STRING_LICENSING_EXCEPTION_ALIAS_1)
			  + e.getMessage());
		} catch (UnsatisfiedLinkError e) {
		  // Make sure the native FlxCore library is available
		  System.out.println(tra.get_string(Calculator_tra.TRA_STRING_NO_FLXCORE_ALIAS_1));
		} finally {
		  if (licensing != null) {
			// Clean up resources
			licensing.close();
		  }
		}
	  }
	});
  }

  private static void usage() {
	System.out.println("\nCalculator [binary_license_file]"
		+ "\nAttempts to acquire highres' features from a license source read from a file "
		+ "\nIf unset, default binary_license_file is license.bin.");
  }

  private static boolean validateArgs(String[] args, Calculator_tra tra) {
	if (args.length > 1) {
	  usage();
	  return false;
	} else if (args.length == 1) {
	  if (args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
		usage();
		return false;
	  } else {
		licenseFile = args[0];
		System.out.println("Using license file " + licenseFile + ".");
	  }
	} else {
	  licenseFile = tra.get_string(Calculator_tra.TRA_STRING_LICENSE_BIN_ALIAS_1);
	  System.out.println("Using default license file " + licenseFile + ".");
	}
	return true;
  }

  public static class BinaryMessage {

	/**
	 * Read binary from file
	 *
	 * @param inputFile File path including the name to be read
	 * @return data as byte array
	 */
	public static byte[] readData(String inputFile) {

	  byte[] responseData = null;
	  try (FileInputStream idfile = new FileInputStream(inputFile)) {
		responseData = new byte[(int) (idfile.getChannel().size())];
		idfile.read(responseData);
	  } catch (FileNotFoundException e) {
		System.out.println("Unable to find file " + inputFile + ".");
	  } catch (IOException e) {
		System.out.println("Error reading data from file " + inputFile + ".");
	  }
	  return responseData;
	}

	/**
	 * Write buffer to output file
	 *
	 * @param outputFile File path and name
	 * @param buffer Buffer to be written into the file
	 */
	public static void writeData(String outputFile, byte[] buffer) {

	  try (FileOutputStream fos = new FileOutputStream(outputFile)) {
		fos.write(buffer);
	  } catch (IOException e) {
		System.out.println("Error writing file " + outputFile + ": " + e.getMessage());
	  }
	}
  }
}

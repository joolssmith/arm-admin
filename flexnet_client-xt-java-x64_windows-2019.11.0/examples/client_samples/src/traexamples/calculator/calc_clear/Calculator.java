/**
 * Copyright (c) 2011-2019 Flexera Software LLC. All Rights Reserved. This software has been
 * provided pursuant to a License Agreement containing restrictions on its use. This software
 * contains valuable trade secrets and proprietary information of Flexera Software LLC and is
 * protected by law. It may not be copied or distributed in any form or medium, disclosed to third
 * parties, reverse engineered or used in any manner not provided for in said License Agreement
 * except with the prior written authorization from Flexera Software LLC.
 */
package calc_clear;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.swing.*;
import javax.swing.JOptionPane;
import java.awt.*;
import java.awt.event.*;
import org.apache.log4j.Logger;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;


public class Calculator extends JFrame {

  // This example program allows you to:
  // 1. Acquire feature "highres" license from license source read from a file .
  // 2. Invokes a simple calculator application

  private static final long serialVersionUID = 1L;
  static Logger logger = Logger.getLogger(Calculator.class);
  static String lastCommand = new String();
  double res = 0;
  JPanel panel = new JPanel();
  JTextField result = new JTextField("0", 45);
  private boolean start;
  private static String licenseFile = "license.bin";

  boolean licensed = false;

  public Calculator() {
	setLayout(new BorderLayout());
	result.setEditable(false);
	add(result, BorderLayout.NORTH);
	panel.setLayout(new GridLayout(4, 4));
	start = true;

	ActionListener insert = new InsertAction();
	ActionListener command = new CommandAction();

	addButton("7", insert);
	addButton("8", insert);
	addButton("9", insert);
	addButton("/", command);

	addButton("4", insert);
	addButton("5", insert);
	addButton("6", insert);
	addButton("*", command);

	addButton("1", insert);
	addButton("2", insert);
	addButton("3", insert);
	addButton("-", command);

	addButton("0", insert);
	addButton(".", insert);
	addButton("=", command);
	addButton("+", command);
	add(panel, BorderLayout.CENTER);
	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }

  /**
   * This action inserts the button action string to the end of the display text.
   */
  private class InsertAction implements ActionListener {
	public void actionPerformed(ActionEvent event) {
	  String input = event.getActionCommand();
	  if (start) {
		result.setText("");
		start = false;
	  }
	  result.setText(result.getText() + input);
	}
  }

  /**
   * This action executes the command that the button action string denotes.
   */
  private class CommandAction implements ActionListener {
	public void actionPerformed(ActionEvent event) {
	  String command = event.getActionCommand();
	  if (start) {
		lastCommand = command;
	  } else {
		calculate(Double.parseDouble(result.getText()));
		lastCommand = command;
		start = true;
	  }
	}
  }

  /**
   * Adds a button to the center panel.
   *
   * @param label the button label
   * @param listener the button listener
   */
  private void addButton(String label, ActionListener listener) {
	JButton button = new JButton(label);
	button.addActionListener(listener);
	panel.add(button);
  }

  /**
   * Carries out the pending calculation.
   *
   * @param x the value to be accumulated with the prior result.
   */
  public void calculate(double x) {
	if (!lastCommand.isEmpty()) {
	  logger.debug("Entered Input : " + res + " " + lastCommand + " " + x);
	}

	if (lastCommand.equals("+"))
	  res += x;
	else if (lastCommand.equals("-"))
	  res -= x;
	else if (lastCommand.equals("*")) {
	  if (licensed) {
		res *= x;
	  } else {
		JOptionPane.showMessageDialog(this, "Multiply not licensed", "alert",
			JOptionPane.ERROR_MESSAGE);
	  }
	} else if (lastCommand.equals("/")) {
	  if (licensed) {
		res /= x;
	  } else {
		JOptionPane.showMessageDialog(this, "Divide not licensed", "alert",
			JOptionPane.ERROR_MESSAGE);
	  }
	} else if (lastCommand.equals("=") || lastCommand.isEmpty()) {
	  res = x;
	}
	result.setText("" + res);

	if (!lastCommand.isEmpty()) {
	  logger.debug("Result : " + result.getText());
	}
  }



  public static void main(String[] args) {

	// Validate command-line arguments
	if (!validateArgs(args)) {
	  return;
	}
	if (flxexamples.IdentityClient.IDENTITY_DATA == null) {
	  System.out.println("License-enabled code requires client identity data, "
		  + "which you create with pubidutil and printbin -java. "
		  + "See the User Guide for more information.");
	  return;
	}
	EventQueue.invokeLater(new Runnable() {
	  public void run() {
		Calculator calc = new Calculator();
		calc.setTitle("Calculator");
		calc.setSize(241, 217);
		calc.setLocation(400, 250);
		calc.setVisible(true);
		ILicensing licensing = null;

		try {

		  // Initialize ILicensing interface with identity data using file-based trusted storage in
		  // user's home directory and hard-coded string hostid "1234567890"
		  licensing =
			  LicensingFactory.getLicensing(flxexamples.IdentityClient.IDENTITY_DATA,
				  System.getProperty("user.home"), "1234567890", "basicclient");
		  // Get ILicenseManager interface, the primary object for licensing-related functionality
		  ILicenseManager licenseManager = licensing.getLicenseManager();
		  // Add buffer license source
		  System.out.println("Reading data from " + licenseFile + ".");
		  licenseManager.addBufferLicenseSource(BinaryMessage.readData(licenseFile));

		  // Acquire 1 "highres" license
		  acquireLicense(licenseManager, "highres", "1.0", 1, calc);
		} catch (FlxException e) {
		  System.out.println("Licensing exception: " + e.getMessage());
		} catch (UnsatisfiedLinkError e) {
		  // Make sure the native FlxCore library is available
		  System.out
			  .println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		} finally {
		  if (licensing != null) {
			// Clean up resources
			licensing.close();
		  }
		}
	  }
	});
  }

  private static void usage() {
	System.out.println("\nCalculator [binary_license_file]"
		+ "\nAttempts to acquire highres' features from a license source read from a file "
		+ "\nIf unset, default binary_license_file is license.bin.");
  }

  private static boolean validateArgs(String[] args) {
	if (args.length > 1) {
	  usage();
	  return false;
	} else if (args.length == 1) {
	  if (args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
		usage();
		return false;
	  } else {
		licenseFile = args[0];
		System.out.println("Using license file " + licenseFile + ".");
	  }
	} else {
	  System.out.println("Using default license file " + licenseFile + ".");
	}
	return true;
  }

  public static class BinaryMessage {

	/**
	 * Read binary from file
	 *
	 * @param inputFile File path including the name to be read
	 * @return data as byte array
	 */
	public static byte[] readData(String inputFile) {

	  byte[] responseData = null;
	  try (FileInputStream idfile = new FileInputStream(inputFile)) {
		responseData = new byte[(int) (idfile.getChannel().size())];
		idfile.read(responseData);
	  } catch (FileNotFoundException e) {
		System.out.println("Unable to find file " + inputFile + ".");
	  } catch (IOException e) {
		System.out.println("Error reading data from file " + inputFile + ".");
	  }
	  return responseData;
	}

	/**
	 * Write buffer to output file
	 *
	 * @param outputFile File path and name
	 * @param buffer Buffer to be written into the file
	 */
	public static void writeData(String outputFile, byte[] buffer) {

	  try (FileOutputStream fos = new FileOutputStream(outputFile)) {
		fos.write(buffer);
	  } catch (IOException e) {
		System.out.println("Error writing file " + outputFile + ": " + e.getMessage());
	  }
	}
  }


  private static void acquireLicense(ILicenseManager licenseManager, String name, String version,
	  int count, Calculator calc) {
	ILicense license = null;
	try {
	  license = licenseManager.acquire(name, version, count);
	  System.out.println("Successfully acquired \"" + license.getName() + "\", version "
		  + license.getVersion() + ", " + license.getCount() + " count.");
	  try {
		// return license
		licenseManager.returnLicense(license);
		calc.licensed = true;
	  } catch (FlxException e) {
		System.out.println("Unable to return " + name + ": " + e.getMessage());
	  }
	} catch (FlxException e) {
	  System.out.println("Unable to acquire " + name + ": " + e.getMessage());
	}
  }

}

/**
 * Copyright (c) 2011-2019 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;

public class Client {

	//	This example program enables you to:
	//	1. Process the license file of your choice.
	//	2. Acquire licenses from a combination of buffer, trusted storage, and
	//	   trial license sources.
	
	private static String inputFile = null;
	
	public static void main(String[] args) {

		// Validate command-line arguments
		if(!validateArgs(args)) {
			return;
		}
		
		if(IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "client")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();
			
			// Add buffer license source
			if(inputFile != null) {
				System.out.println("Reading data from " + inputFile + ".");
				licenseManager.addBufferLicenseSource(Files.readAllBytes(Paths.get(inputFile)));
				// Get valid features from buffer license source
				List<IFeature> bufferFeatures = licenseManager.getFeaturesFromBuffer(Files.readAllBytes(Paths.get(inputFile)), false);
				System.out.println("Number of features loaded from buffer: " + bufferFeatures.size() + ".");
			}
			else {
				System.out.println("No license file specified.");
			}
			
			// Add trusted storage license source
			licenseManager.addTrustedStorageLicenseSource();
			// Get valid features from trusted storage license source
			List<IFeature> tsFeatures = licenseManager.getFeaturesFromTrustedStorage(false);
			System.out.println("Number of features loaded from trusted storage: " + tsFeatures.size() + ".");
			
			// Add trial storage license source
			licenseManager.addTrialLicenseSource();
			// Get features from trial storage license source
			List<IFeature> trialFeatures = licenseManager.getFeaturesFromTrials();
			System.out.println("Number of features loaded from trial storage: " + trialFeatures.size() + ".");
			
			// Acquire 1 "survey" license
			acquireLicense(licenseManager, "survey", "1.0", 1);
			
			// Acquire 1 "highres" license
			acquireLicense(licenseManager, "highres", "1.0", 1);
			
			// Acquire 1 "lowres" license
			acquireLicense(licenseManager, "lowres", "1.0", 1);

			// Acquire 1 "download" license
			acquireLicense(licenseManager, "download", "1.0", 1);

			// Acquire 1 "upload" license
			acquireLicense(licenseManager, "upload", "1.0", 1);
			
			// Acquire 1 "updates" license
			acquireLicense(licenseManager, "updates", "1.0", 1);
			
			// Acquire 1 "special" license
			acquireLicense(licenseManager, "special", "1.0", 1);

			// Acquire 100 "sdchannel" licenses
			acquireLicense(licenseManager, "sdchannel", "1.0", 100);
			
			// Acquire 10 "hdchannel" licenses
			acquireLicense(licenseManager, "hdchannel", "1.0", 10);
		}
		catch(FlxException e) {
			System.out.println("Licensing exception: " + e.getMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
		catch (NoSuchFileException e) {
			System.out.println("No such file: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}	
	
	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " + 
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return license
				licenseManager.returnLicense(license);
			}
			catch (FlxException e) {
				System.out.println("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			System.out.println("Unable to acquire " + name + ": " + e.getMessage());
		}
	}
	
	private static void usage() {
		System.out.println(
        "\nClient [binary_license_file]" +
        "\nAttempts to acquire various features from binary license file, " +
        "\ntrusted storage, and trial license sources. ");
		
	}
	
	private static boolean validateArgs(String[] args) {
		if(args.length > 1) {
			usage();
			return false;
		}
		else if (args.length == 1) {
			if(args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
				usage();
				return false;
			}
			else {
				inputFile = args[0];
			}
		}		
		return true;

	}
}

/**
 * Copyright (c) 2011-2019 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.flexnet.licensing.LicensingConstants;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.IFeature.AcquisitionStatus;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.HostIdType;

public class View {

	//	This example program enables you to:
	//	1. View features presented in the license file and trusted storage.
	//	2. Find out the validity status of the features.
	
	private static String licenseFile = null;
	
	public static void main(String[] args) {
		
		// Validate command-line arguments
		if(!validateArgs(args)) {
			return;
		}
		
		if(IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "view")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();

			if (licenseManager.isContainerized()){
				System.out.println("Client is contained in a docker host");
			}
			else {
				System.out.println("Client is not contained in a docker host");
			}

			// One day in seconds
			final int tolerance = 86400;
			final int frequency = 86400;

			// Enable clock windback detection
			licenseManager.enableWindbackDetection(tolerance, frequency);

			// Buffer feature info if present
			if(licenseFile != null) {
				System.out.println("Reading data from " + licenseFile + ".");
				List<IFeature> bufferFeatures = licenseManager.getFeaturesFromBuffer(licenseFile, true);
				System.out.println("==============================================");
				System.out.println("Features found in " + licenseFile);
				displayFeatures(bufferFeatures);
				System.out.println("==============================================");
			}
			
			// Trusted storage feature info
			List<IFeature> tsFeatures = licenseManager.getFeaturesFromTrustedStorage(true);
			System.out.println("==============================================");
			System.out.println("Features found in trusted storage");
			displayFeatures(tsFeatures);
			System.out.println("==============================================");
			
			// Trial storage feature info
			List<IFeature> trialFeatures = licenseManager.getFeaturesFromTrials();
			System.out.println("==============================================");
			System.out.println("Features found in trial storage");
			displayFeatures(trialFeatures);
			System.out.println("==============================================");
			
			// Short code storage feature info
			List<IFeature> scFeatures = licenseManager.getFeaturesFromShortCode(true);
		    System.out.println("==============================================");
	        System.out.println("Features found in short-code storage");
	        displayFeatures(scFeatures);
	        System.out.println("==============================================");
		}
		catch (FlxException e) {
			System.out.println("Licensing exception: " + e.getMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
	}

	private static void displayFeatures(List<IFeature> features) {

		StringBuilder info = null;
		DateFormat format = DateFormat.getDateInstance();
		
		if(features.size() == 0) {
			System.out.println("*NONE*");
			return;
		}
		
		for(IFeature feature: features) {
			info = new StringBuilder();
			// Name
			info.append(feature.getName() + " ");
			// Version
			info.append(feature.getVersion() + " ");
			// Expiration
			Date expiration = feature.getExpiration();
			if(expiration != null) {
				info.append(format.format(expiration));
			} else {
				info.append("permanent");
			}
			// Count
			if(LicensingConstants.UNCOUNTED == feature.getCount()) {
				info.append(" uncounted");
			} else {
				info.append(" " + Long.toString(feature.getCount()));
			}
			// Hostid(s)
			Map<HostIdType, List<String>> hostIds = feature.getHostIds();
			if(hostIds.size() > 0) {
			    int numOfHostIds = 0;
	            String hostIdString = "";
	            for(Map.Entry<HostIdType, List<String>> type : hostIds.entrySet()) {
	                if(type.getKey() != HostIdType.ANY) {
	                    hostIdString += type.getKey().toString() + "=";
	                }
                    boolean first = true;
                    String value = "";
                    int numOfType = 0;
                    for(String id : type.getValue()) {
                        if(first) {
                            first = false;
                            value = id;
                        }
                        else {
                            value += "," + id;
                        }
                        numOfType++;
                        numOfHostIds++;
                    }
                    if(numOfType > 1) {
                        hostIdString += "(" + value + ")";	                        
                    }
                    else {
                        hostIdString += value;
                    }
	                hostIdString += " ";
	            }
	            if(numOfHostIds == 1) {
	                info.append(" Hostid=" + hostIdString.trim());
	            }
	            else {
	                info.append(" Hostids=[" + hostIdString.trim() + "]");
	            }
			}
			// Notice
			if(feature.getNotice() != null) {
				info.append(" NOTICE=\"" + feature.getNotice() + "\"");
			}
			// Issuer
			if(feature.getIssuer() != null) {
				info.append(" ISSUER=\"" + feature.getIssuer() + "\"");
			}
			// Vendor string
			if(feature.getVendorString() != null) {
				info.append(" VENDOR_STRING=\"" + feature.getVendorString() + "\"");
			}
			// Serial number
			if(feature.getSerialNumber() != null) {
				info.append(" SN=\"" + feature.getSerialNumber()  + "\"");
			}
			// Issued date
			Date issued = feature.getIssued();
			if(issued != null) {
				info.append(" ISSUED=\"" + format.format(issued)  + "\"");				
			}
			// Start date
			Date startDate = feature.getStartDate();
			if(startDate != null) {
				info.append(" START=\"" + format.format(startDate) + "\"");
			}
			// Metered REUSABLE
			if (feature.isMeteredReusable()) {
				info.append(" MODEL=metered REUSABLE");
			} // Metered
			else if (feature.isMetered()) {
				if (feature.getMeteredUndoInterval() > 0) {
					info.append(" MODEL=metered UNDO_INTERVAL=" + feature.getMeteredUndoInterval());
				}
				else {
					info.append(" MODEL=metered");
				}
			}
			// Acquisition status
			AcquisitionStatus status = feature.getAcquisitionStatus();
			if(status != null && status == AcquisitionStatus.VALID) {
				if (feature.isMetered()) {
					long available = feature.getAvailableAcquisitionCount();
					if (available == 0) {
						info.append(", Entire count consumed");
					}
					else if (available == LicensingConstants.UNCOUNTED) {
						info.append(", Valid for acquisition");
					}
					else {
						info.append(", Available for acquisition: " + available);
					}
				}
				else {
					info.append(", Valid for acquisition");
				}
			}
			else {
				info.append(", Not valid for acquisition: " + getAcquisitionStatusString(status));
			}
			
			System.out.println(info.toString());
		}
	}
	
	private static String getAcquisitionStatusString(AcquisitionStatus status) {
		String acqStatus = null;
		
		if(status == AcquisitionStatus.VALID)
			acqStatus = "Valid for acquisition";
		else {
			switch(status) {
			case EXPIRED:
				acqStatus = "Requested feature has expired.";
				break;
			case HOST_ID_MISMATCH:
				acqStatus = "Requested feature's hostid does not match system hostid.";
				break;
			case NOT_STARTED:
				acqStatus = "Start date for the requested feature is in the future.";
				break;
			case WINDBACK_DETECTED:
				acqStatus = "Clock windback detected.";
				break;
			case VENDOR_NAME_INVALID:
				acqStatus = "Feature was issued by a different vendor.";
				break;
			case SIGNATURE_INVALID:
				acqStatus = "Signature didn't pass validation.";
				break;
			case TS_HOST_ID_MISMATCH:
				acqStatus = "Trusted storage hostid does not match system hostid.";
				break;
			case UNKNOWN_KEYWORD:
				acqStatus = "Unknown certificate keyword.";
				break;
			case UNSUPPORTED_KEYWORD:
				acqStatus = "Unsupported certificate keyword.";
				break;
			default:
				acqStatus = "General feature error.";	
			}
		}
		
		return acqStatus;
	}

	private static boolean validateArgs(String[] args) {
		if(args.length > 1) {
			usage();
			return false;
		}
		else if (args.length == 1) {
			if(args[0].equalsIgnoreCase("-h") || args[0].equalsIgnoreCase("-help")) {
				usage();
				return false;
			}
			else {
				licenseFile = args[0];
			}
		}		
		return true;
	}
	
	private static void usage() {
		System.out.println(
				"\nView [binary_license_file]" +
		    	"\nDisplays feature keyword information and validity based on contents " +
		    	"\nof a binary license file and trusted storage, when available.");
	}
}

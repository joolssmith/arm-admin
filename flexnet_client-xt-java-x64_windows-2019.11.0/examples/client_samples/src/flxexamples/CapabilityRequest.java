/**
 * Copyright (c) 2011-2019 Flexera Software LLC.
 * All Rights Reserved.
 * This software has been provided pursuant to a License Agreement
 * containing restrictions on its use.  This software contains
 * valuable trade secrets and proprietary information of
 * Flexera Software LLC and is protected by law.
 * It may not be copied or distributed in any form or medium, disclosed
 * to third parties, reverse engineered or used in any manner not
 * provided for in said License Agreement except with the prior
 * written authorization from Flexera Software LLC.
 */

package flxexamples;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import com.flexnet.licensing.client.ICapabilityRequestOptions;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IResponseStatus;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.SharedConstants.MachineType;
import com.flexnet.lm.SharedConstants.PropStatusCode;
import com.flexnet.lm.SharedConstants.RequestOperation;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.net.Comm;
import com.flexnet.lm.resources.Resources;


public class CapabilityRequest {

	private static boolean generateRequest;
	private static boolean storeRequest;
	private static boolean processResponse;
	private static boolean server;
	private static boolean readFromFile;


	//	This example program enables you to:
	//	1. Send a capability request over HTTP to the server and process the response,
	//	   saving data into trusted storage.
	//	2. Write a capability request to a file. This request can be fed into a server
	//	   to generate a response.
	//	3. Read a capability response from a file and process accordingly.

	public static void main(String[] args) {

		// Validate command-line arguments
		if(!validateArgs(args)) {
			return;
		}
		
		if(IdentityClient.IDENTITY_DATA == null) {
			System.out.println("License-enabled code requires client identity data, " +
								"which you create with pubidutil and printbin -java. " +
								"See the User Guide for more information.");
			return;
		}

		// Initialize ILicensing interface with identity data using file-based trusted storage in
		// user's home directory and hard-coded string hostid "1234567890" inside
		// a try-with-resources block
		try (ILicensing licensing = LicensingFactory.getLicensing(IdentityClient.IDENTITY_DATA, System.getProperty("user.home"), "1234567890", "capabilityrequest")) {
			// Get ILicenseManager interface, the primary object for licensing-related functionality
			ILicenseManager licenseManager = licensing.getLicenseManager();
            // The optional host name is typically set by a user as a friendly name for the host.  
            // The host name is not used for license enforcement.			
			licenseManager.setHostName("Sample Device");
			// The host type is typically a name set by the implementer, and is not modifiable by the user.
			// While optional, the host type may be used in certain scenarios by some back-office systems such as FlexNet Operations.
			licenseManager.setHostType("FLX_CLIENT");
			// Existing features from trusted storage
			List<IFeature> existingFeatures = licenseManager.getFeaturesFromTrustedStorage(false);
			System.out.println("Number of features loaded from trusted storage: " + existingFeatures.size());
			
			byte[]			requestData = null;
			byte[]			responseData = null;

			// Generate a capability request
			if(generateRequest) {
				// Get request options
				ICapabilityRequestOptions	options = licenseManager.createCapabilityRequestOptions();
				// Force response
				options.forceResponse();
				// Optional vendor dictionary
				options.addVendorDictionaryItem("key1", "one");
				options.addVendorDictionaryItem("key2", 2);
				
				// Requesting licenses.
				//
				// Add requested license information here, such as desired features or rights IDs
				//
				// options.addRightsId("ACT_TEST", 1);
				// options.addDesiredFeature("survey", "1.0", 1);
				//
				// Incremental capability requests.
				//
				// When desired features are used in conjunction with an incremental capability request
				// (see the API documentation for ICapabilityRequestOptions.setIncremental), the feature 
				// count specified is a request for either an addition to (positive count value) or a
				// subtraction from (negative count value) the current served count for the specified
				// feature information. 
				//
				// Previewing available licenses.
				//
				// For a preview capability request, set operation to RequestOperation.PREVIEW
				// and add specific features to preview using addDesiredFeature() or specify
				// to preview all features using requestAllFeatures().
				//

				// Generate request
				requestData = licenseManager.generateCapabilityRequest(options);
				if(storeRequest) {
					Files.write(Paths.get(args[1]), requestData, StandardOpenOption.CREATE_NEW);
					System.out.println("Successfully generated request to " + args[1] + ".");
				}
			}
			// Communicate with server
			if(server) {
				responseData = talkToServer(requestData, args[1]);
			}

			// Read response from file
			if(readFromFile) {
				System.out.println("Reading response from " + args[1] + ".");
				responseData = Files.readAllBytes(Paths.get(args[1]));
			}

			if(processResponse && (responseData != null) && (responseData.length > 0)) {
				// Process response
				ICapabilityResponseData responseDetails = licenseManager.processCapabilityResponse(responseData);
				System.out.println("Successfully processed response.");
				if(responseDetails != null) {
					// Get response details
					getResponseDetails(responseDetails);
				}
	            // Features in processed response
	            List<IFeature> features = licenseManager.getFeaturesFromTrustedStorage(false);
	            System.out.println("Number of features loaded from capability response: " + features.size());

				// Acquire 1 "survey" license
				acquireLicense(licenseManager, "survey", "1.0", 1);
			}
		}
		catch (FlxException e) {
			System.out.println("Licensing exception: " + e.getDiagnosticMessage());
		}
		catch (UnsatisfiedLinkError e) {
			// Make sure the native FlxCore library is available
			System.out.println("Failed to load FlxCore library. Ensure it's in PATH (Windows) or LD_LIBRARY_PATH (other platforms).");
		}
		catch (NoSuchFileException e) {
			System.out.println("No such file: " + e.getMessage());
		}
		catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}
	}

	private static void getResponseDetails(ICapabilityResponseData responseDetails) {
		try {
			System.out.println("Response data");

			// Vendor dictionary
			System.out.println("Vendor dictionary:");
			Map<String, Object>	vendorDictionary = responseDetails.getVendorDictionary();
			if(vendorDictionary.size() > 0) {
				Set<String> keys = vendorDictionary.keySet();
				for(String key : keys) {
					Object	value = vendorDictionary.get(key);
					System.out.println("\t" + key + "=" + value.toString());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Status code
			System.out.println("Response status:");
			List<IResponseStatus> statusCodes = responseDetails.getResponseStatus();
			if(statusCodes.size() > 0) {
				for(IResponseStatus status : statusCodes) {
					PropStatusCode code = PropStatusCode.findId(status.getCode());
					String description = Resources.getString(code.getKey());
					System.out.println("\t" + "category: " + status.getCategory().toString().toLowerCase() + 
							", code: " + status.getCode() + (description == null ? "" : " (" + description + ")") +
							", details: " + status.getDetails());
				}
			}
			else {
				System.out.println("\tnone");
			}

			// Virtual machine info
			System.out.println("Machine Type:");
			MachineType	machineType = responseDetails.getVirtualMachineType();
			if(machineType == MachineType.VIRTUAL) {
				System.out.println("\tVIRTUAL");
				Map<String, Object> vmInfo = responseDetails.getVirtualMachineInfo();
				if(vmInfo.size() > 0) {
					Set<String> keys = vmInfo.keySet();
					for(String key : keys) {
						Object value = vmInfo.get(key);
						System.out.println("\t\t" + key + "=" + value.toString());
					}
				}
			}
			else if(machineType == MachineType.PHYSICAL) {
				System.out.println("\tPHYSICAL");
			}
			else {
				System.out.println("\tUNKNOWN");
			}
			
			// Confirmation request
			System.out.println("Confirmation request needed: " + 
					(responseDetails.getConfirmationRequestNeeded() ? "yes" : "no") );

		}
		catch (FlxException e) {
			System.out.println("Error getting response details.");
		}
	}


	private static byte[] talkToServer(byte[] request, String uri) {
	    byte[] response = null;
	    try {
	        Comm conn = Comm.getHttpInstance(uri);
	        response = conn.sendBinaryMessage(request);
	    }
	    catch (FlxException e) {
	        e.printStackTrace();
	    }
	    return response;
	}

	private static void acquireLicense(ILicenseManager licenseManager, String name, String version, int count) {
		ILicense	license = null;
		try {
			license = licenseManager.acquire(name,  version, count);
			System.out.println("Successfully acquired \"" + license.getName() + "\", version " +
					license.getVersion() +  ", " + license.getCount() + " count.");
			try {
				// return license
				licenseManager.returnLicense(license);
			}
			catch (FlxException e) {
				System.out.println("Unable to return " + name + ": " + e.getMessage());
			}
		}
		catch (FlxException e) {
			System.out.println("Unable to acquire " + name + ": " + e.getMessage());
		}
	}

	private static void usage() {
		System.out.println(
				"\nCapabilityRequest [-generate outputfile]" +
				"\nCapabilityRequest [-process inputfile]" +
				"\nCapabilityRequest [-server url]\n" +
				"\nwhere:" +
				"\n-generate    Generate capability request into a file." +
				"\n-process     Processes capability response from a file." +
				"\n-server      Sends request to a server and processes the response." +
		        "\n             For the test back-office server, use http://hostname:8080/request." +
		        "\n             For FlexNet Operations, use http://hostname:8888/flexnet/deviceservices." +
		        "\n             For FlexNet Embedded License Server, use http://hostname:7070/fne/bin/capability." +
		        "\n             For Cloud License Server, use https://<tenant>.compliance.flexnetoperations.com/instances/<instance-id>/request." +
		        "\n             For FNO Cloud, use https://<tenant>.compliance.flexnetoperations.com/deviceservices.");
	}

	private static boolean validateArgs(String[] args) {
		if (args.length > 0) {
			if ( args[0].equalsIgnoreCase("-generate") ) {
				if (args.length == 2) {
					generateRequest = true;
					storeRequest = true;
					return true;
				}
			}
			else if( args[0].equalsIgnoreCase("-process") ) {
				if (args.length == 2) {
					processResponse = true;
					readFromFile = true;
					return true;
				}
			}
			else if( args[0].equalsIgnoreCase("-server") ) {
				if (args.length == 2) {
					generateRequest = true;
					processResponse = true;
					server = true;
					return true;
				}
			}
			else if (!args[0].equalsIgnoreCase("-h") && !args[0].equalsIgnoreCase("-help")) {
				System.out.println("unknown option: " + args[0]);
			}
		}
		usage();
		return false;
	}

}

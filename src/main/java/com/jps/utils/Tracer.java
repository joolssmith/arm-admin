//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * @author juliansmith 2018
 *
 */
public class Tracer {
  @SuppressWarnings("serial")
  public final class Abort extends RuntimeException {
    public Abort(final String message) {
      super(message);
    }

    public Abort(final Throwable t) {
      super(t);
    }
  }

  public final static ObjectMapper json_mapper = new ObjectMapper(new JsonFactory())
      .configure(SerializationFeature.INDENT_OUTPUT, true)
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  public final static ObjectMapper yaml_mapper = new ObjectMapper(new YAMLFactory())
      .configure(SerializationFeature.INDENT_OUTPUT, true)
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS, false)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  final Logger logger;
  
  private Tracer(final Class<?> clazz) {
    this.logger = LoggerFactory.getLogger(clazz.getName());
  }

  public void debug(final String format, final Object... args) {
    this.logger.debug(String.format(format, args));
  }

  public void info(final String format, final Object... args) {
    this.logger.info(String.format(format, args));
  }
  
  public void trace(final String format, final Object... args) {
    this.logger.trace(String.format(format, args));
  }
    
  public void warn(final String format, final Object... args) {
    this.logger.warn(String.format(format, args));
  }
  
  public void error(final String format, final Object... args) {
    this.logger.error(String.format(format, args));
  }
  
  public void exception(final Throwable t) {
    this.logger.error(String.format("%s: %s", t.getClass().getName(), t.getMessage()));
  }

  public void in() {
    debug(">> " + Thread.currentThread().getStackTrace()[2].getMethodName());
  }

  public void out() {
    debug("<< " + Thread.currentThread().getStackTrace()[2].getMethodName());
  }

  public void json(final Object obj) {
    try {
      info("%s: %s", obj.getClass().getName(), json_mapper.writeValueAsString(obj));
    }
    catch (final JsonProcessingException e) {
      exception(e);
    }
  }

  public void yaml(final Object obj) {
    try {
      info("%s: %s", obj.getClass().getName(), yaml_mapper.writeValueAsString(obj));
    }
    catch (final JsonProcessingException e) {
      exception(e);
    }
  }

  public void abort() {
    throw new Abort("abort");
  }

  public static Tracer create(final Class<?> clazz) {
    return new Tracer(clazz);
  }
}
//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

/**
 * @author juliansmith
 *
 */
public class AuthorizationRequest {
  public String password;
  public String user;
}

//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.reservations;

/**
 * @author juliansmith
 *
 */
public class HostId {
  public String value;
  public String type;
}

//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

/**
 * @author juliansmith 2020
 *
 */
public class PostAuthorizationResponse {
  public String expires;
  public String token;
}

//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

import com.jps.arm.poc.cls.health.Health;

public class GetHealthResponse {
  public Health LLS; 
  public Health GLS; 
}
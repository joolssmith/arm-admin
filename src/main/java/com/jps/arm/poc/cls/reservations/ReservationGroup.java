//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.reservations;

/**
 * @author juliansmith 2020
 *
 */
public class ReservationGroup {
  public String name;
  public Reservation[] reservations;
}

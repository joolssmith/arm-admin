//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.health;

public class Health {
  public String version;
  public String buildDate;
  public String buildVersion;
  public String branch;
  public String patch;
  public String fneBuildVersion;
  public String serverInstanceID;
  public Database database;
  public String security;
  public String httpAuth;
  public String licensingSecurityJSON;
  public String diskspace;
}
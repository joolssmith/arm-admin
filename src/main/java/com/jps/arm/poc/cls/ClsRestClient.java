//**********************************************
// Copyright (c) 2020 Flexera Software LLC
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jps.arm.poc.cls.features.Feature;
import com.jps.arm.poc.cls.reservations.Reservation;
import com.jps.arm.poc.cls.reservations.ReservationGroup;

/**
 * @author juliansmith
 *
 */
public class ClsRestClient {

  private static ObjectMapper json_mapper = new ObjectMapper()
      .configure(SerializationFeature.INDENT_OUTPUT, false)
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

  private PostAuthorizationResponse postAuthorizationResponse = null;
  private List<Feature> getFeaturesResponse = null;
  private PostReservationGroupResponse postReservationGroupResponse = null;
  private List<GetReservationGroupsResponse> getReservationGroupResponse = null;
  private List<GetReservationsResponse> getReservationsResponse = null;
  private GetHealthResponse getHealthResponse = null;
  private PostReservationGroupResponse postReservationResponse = null;
  
  private final RestTemplate restTemplate = new RestTemplate();

  private String baseUrl;

  private ClsRestClient() {
    
  }
  
  private String url(final String... resource) {
    return this.baseUrl + "/" + StringUtils.join(resource, '/');
  }
  
  private HttpEntity<String> createRequest(final Object payload) throws JsonProcessingException {

    final HttpHeaders headers = new HttpHeaders();

    headers.clear();
    if (Objects.nonNull(this.postAuthorizationResponse)) {
      headers.set("Authorization", "Bearer " + this.postAuthorizationResponse.token);
    }

    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

    return new HttpEntity<String>(json_mapper.writeValueAsString(payload), headers);
  }

  private HttpEntity<String> createRequest() {
    final HttpHeaders headers = new HttpHeaders();

    headers.clear();
    if (Objects.nonNull(this.postAuthorizationResponse)) {
      headers.set("Authorization", "Bearer " + this.postAuthorizationResponse.token);
    }

    return new HttpEntity<String>(headers);
  }
  
  public static ClsRestClient create() {
    return new ClsRestClient();
  }
  
  public PostAuthorizationResponse getPostAuthorizationResponse() {
    return postAuthorizationResponse;
  }

  public PostReservationGroupResponse getPostReservationGroupResponse() {
    return postReservationGroupResponse;
  }

  public List<GetReservationGroupsResponse> getGetReservationGroupResponse() {
    return getReservationGroupResponse;
  }

  public List<GetReservationsResponse> getGetReservationsResponse() {
    return getReservationsResponse;
  }
  
  public List<Feature> getGetFeaturesResponse() {
    return getFeaturesResponse;
  }

  public GetHealthResponse getGetHealthResponse() {
    return getHealthResponse;
  }

  public Object getPostReservationResponse() {
    return postReservationResponse;
  }

  public ClsRestClient withBaseUrlCloud(final String server_host, final String server_id) {
    this.baseUrl = String.format("%s/api/1.0/instances/%s", server_host, server_id);
    return this;
  }

  public ClsRestClient withBaseUrlLocal(final String server_host, final int server_port) {
    this.baseUrl = String.format("%s:%d/api/1.0", server_host, server_port);
    return this;
  }

  public ClsRestClient withBaseUrlLocal(final String server_host, final int server_port, final String server_id) {
    this.baseUrl = String.format("%s:%d/api/1.0/instances/%s", server_host, server_port);
    return this;
  }

  /**
   * 
   * @param user_name
   * @param user_password
   * @return
   * @throws JsonProcessingException
   */
  public HttpStatus authorize(final String user_name, final String user_password) throws JsonProcessingException {

    final HttpEntity<String> request = createRequest(new AuthorizationRequest() {
      {
        this.user = user_name;
        this.password = user_password;
      }
    });

    final ResponseEntity<PostAuthorizationResponse> entity = this.restTemplate.postForEntity(
        url("authorize"), 
        request, 
        PostAuthorizationResponse.class);

    this.postAuthorizationResponse = entity.getBody();
    
    return entity.getStatusCode();
  }

  /**
   * 
   * @return
   * @throws MalformedURLException
   * @throws IOException
   */
  public HttpStatus getFeatures() throws MalformedURLException, IOException {

    final HttpEntity<String> request = createRequest();

    final ResponseEntity<Feature[]> entity = this.restTemplate.exchange(
        url("features"), 
        HttpMethod.GET, 
        request, 
        Feature[].class);

    this.getFeaturesResponse = Arrays.asList(entity.getBody());

    return entity.getStatusCode();
  }

  /**
   * 
   * @return
   * @throws MalformedURLException
   * @throws IOException
   */
  public HttpStatus getReservationGroups() throws MalformedURLException, IOException {

    final HttpEntity<String> request = createRequest();

    final ResponseEntity<GetReservationGroupsResponse[]> entity = this.restTemplate.exchange(
        url("reservationgroups"), 
        HttpMethod.GET, 
        request,
        GetReservationGroupsResponse[].class);

    this.getReservationGroupResponse = Arrays.asList(entity.getBody());

    return entity.getStatusCode();
  }

  /**
   * 
   * @return
   * @throws MalformedURLException
   * @throws IOException
   */
  public HttpStatus getHealth() throws MalformedURLException, IOException {

    final HttpEntity<String> request = createRequest();

    final ResponseEntity<GetHealthResponse> entity = this.restTemplate.exchange(
        url("health"), 
        HttpMethod.GET, 
        request, 
        GetHealthResponse.class);

    this.getHealthResponse = entity.getBody();

    return entity.getStatusCode();
  }

  /**
   * 
   * @param reservation_group_id
   * @return
   * @throws MalformedURLException
   * @throws IOException
   */
  public HttpStatus getReservationGroupReservations(final String reservation_group_id) throws MalformedURLException, IOException {

    final HttpEntity<String> request = createRequest();

    final ResponseEntity<GetReservationsResponse[]> entity = this.restTemplate.exchange(
        url("reservationgroups", reservation_group_id, "reservations"), 
        HttpMethod.GET,
        request, 
        GetReservationsResponse[].class);

    this.getReservationsResponse = Arrays.asList(entity.getBody());

    return entity.getStatusCode();
  }

  public HttpStatus postReservation(final String reservation_group_id, final Reservation reservation) throws MalformedURLException, IOException {

    final HttpEntity<String> request = createRequest(reservation);

    final ResponseEntity<PostReservationGroupResponse> entity = this.restTemplate.postForEntity(
        url("reservationgroups", reservation_group_id), 
        request,
        PostReservationGroupResponse.class);

    this.postReservationResponse = entity.getBody();

    return entity.getStatusCode();
  }
  
  /**
   * 
   * @param reservation_group_id
   * @throws MalformedURLException
   * @throws JsonProcessingException
   */
  public void deleteReservationGroup(final String reservation_group_id) throws MalformedURLException, JsonProcessingException {
    try {
      this.restTemplate.exchange(
          url("reservationgroups", reservation_group_id), 
          HttpMethod.DELETE, 
          createRequest(), 
          Void.class);
    }
    catch (final HttpClientErrorException.Gone e) {
      // actually not an error!
    }
    finally {
    }
  }

  /**
   * 
   * @param reservation_group
   * @return
   * @throws RestClientException
   * @throws MalformedURLException
   * @throws JsonProcessingException
   */
  public HttpStatus postReservationGroup(final ReservationGroup reservation_group)
      throws RestClientException, MalformedURLException, JsonProcessingException {

    final HttpEntity<String> request = createRequest(reservation_group);

    final ResponseEntity<PostReservationGroupResponse> entity = this.restTemplate.postForEntity(
        url("reservationgroups"), 
        request,
        PostReservationGroupResponse.class);

    this.postReservationGroupResponse = entity.getBody();

    return entity.getStatusCode();
  }
}
//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.reservations;

/**
 * @author juliansmith 2020
 *
 */
public class ReservationEntryResponse {
  public String id;
  public String state;
  public String featureName;
  public String featureVersion;
  public String featureCount;
}

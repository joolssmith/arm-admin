//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

import java.util.Date;

/**
 * @author juliansmith
 *
 */
public class GetReservationGroupsResponse {
  public Date creationDate;
  public String ipAddress;
  public String name;
  public String id;
}

//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

/**
 * @author juliansmith 2020
 *
 */
public class PostReservationGroupResponse {
  public String uri;
  public String status;
  
  public String getGroup() {
    final String[] parts = this.uri.split("/");
    
    return parts[parts.length - 1];
  }
}

//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.features;

import java.util.Date;

/**
 * @author juliansmith
 *
 */
public class Feature {
  public String id;
  public String type;
  public String featureName;
  public String featureVersion;
  public Date expiry;
  public String featureCount;
  public String overdraftCount;
  public int used;
  public String vendorString;
  public String issuer;
  public Date issued;
  public String serialNumber;
  public String featureId;
  public Date starts;
  public Date entitlementExpiration;
  public String featureKind;
  public String vendor;
  public int meteredUndoInterval;
  public Boolean meteredReusable;
  public Date receivedTime;
  public String concurrent;
  public Boolean uncounted;
  public Boolean uncappedOverdraft;
  public Boolean metered;
  public String reserved;
  public String borrowInterval;
  public String renewInterval;
}

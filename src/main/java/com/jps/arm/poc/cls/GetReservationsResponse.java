//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls;

import com.jps.arm.poc.cls.reservations.HostId;
import com.jps.arm.poc.cls.reservations.ReservationEntryEx;

/**
 * @author juliansmith
 *
 */
public class GetReservationsResponse {
  public String id;
  public String lastModified;
  public String ipAddress;
  public ReservationEntryEx[] reservationEntries;
  public HostId hostId;
}

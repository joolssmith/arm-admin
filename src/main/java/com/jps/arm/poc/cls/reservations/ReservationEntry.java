//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.cls.reservations;

/**
 * @author juliansmith 2020
 *
 */
public class ReservationEntry {
  public String featureName;
  public String featureVersion;
  public String featureCount;
}



//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.StringUtils;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.HostIdType;
import com.jps.arm.poc.cls.ClsRestClient;
import com.jps.arm.poc.cls.GetReservationGroupsResponse;
import com.jps.arm.poc.cls.features.Feature;
import com.jps.arm.poc.cls.reservations.HostId;
import com.jps.arm.poc.cls.reservations.Reservation;
import com.jps.arm.poc.cls.reservations.ReservationEntry;
import com.jps.arm.poc.cls.reservations.ReservationGroup;
import com.jps.arm.poc.fne.Client;
import com.jps.arm.poc.fne.Client.DeviceType;
import com.jps.arm.poc.fne.DesiredFeature;
import com.jps.arm.poc.fne.MemoryClient;
import com.jps.utils.Tracer;

class Program {
  final static Tracer Trace = Tracer.create(Program.class);
  
  Config config;
  
  static String getProcessId() {
    final String vmName = ManagementFactory.getRuntimeMXBean().getName();
    return vmName.substring(0, vmName.indexOf("@"));
  }
 
  /**
   * Simulates loading a response into in-memory trusted storage
   * 
   * Hostid must match that in the response.
   * 
   * Should then me able to acquire/return features
   * 
   * @throws FlxException
   * @throws JsonProcessingException
   */
  public void memory(final String host_id) throws FlxException, JsonProcessingException {
    final MemoryClient client = new MemoryClient();
    
    Trace.debug("initilialize in-memory client");
    client.initialize(config.identityFilename, host_id, DeviceType.CLIENT, "compiler-" + getProcessId());
    
    Trace.debug("process capability response");
    final ICapabilityResponseData activateResponse = client.activate(config.responseFilename);
    Trace.yaml(activateResponse);
    
    Trace.debug("Get featires in trusted storage");
    final List<IFeature> features = client.getFeaturesInTrustedStorage();
    Trace.yaml(features);
    
    final List<DesiredFeature> desiredFeatures = activateResponse.getFeatures().stream().map(f -> new DesiredFeature() {
      {
        this.name = f.getName();
        this.version = f.getVersion();
        this.count = 1;
      }
    }).collect(Collectors.toCollection(ArrayList::new));
    
    /** acquire all features in TS */
    for (final DesiredFeature feature : desiredFeatures) {
      Trace.json(client.acquire(feature));
    }
    
    /** now release them all */
    client.releaseAll();
  }
  
  public void reservations(final String user_name, final String part_number, final int device_count) throws MalformedURLException, IOException, InterruptedException {
    Trace.debug("create rest client");
    final ClsRestClient clsClient = ClsRestClient
        .create()
        .withBaseUrlCloud(config.fnoHost, config.clsId);
    
    Trace.debug("authorize");
    clsClient.authorize(config.clsAdminUsername, config.clsAdminPassword);
    Trace.yaml(clsClient.getPostAuthorizationResponse());

    Trace.debug("health");
    clsClient.getHealth();
    Trace.yaml(clsClient.getGetHealthResponse());
    
    Trace.debug("poll features");
    for (int i = 0; CollectionUtils.isEmpty(clsClient.getGetFeaturesResponse()); i++) {
      if (i > 0) {
        System.out.println("retry " + i);
        Thread.sleep(1000 * Math.max(i, 5));
      }
      clsClient.getFeatures();
    }
    Trace.yaml(clsClient.getGetFeaturesResponse().toArray());

    // only use features that match this part number
    final List<Feature> features = clsClient.getGetFeaturesResponse()
        .stream()
        .filter(feature -> StringUtils.equals(feature.vendorString, part_number))
        .collect(Collectors.toList());
    
    if (CollectionUtils.isEmpty(features)) {
      throw new RuntimeException("no features match part number " + part_number);
    }
    
    Trace.debug("get reservations groups");
    clsClient.getReservationGroups();
    Trace.yaml(clsClient.getGetReservationGroupResponse().toArray());
    
    // the new reservation
    final Reservation reservation = new Reservation() {
      {
        this.hostId = new HostId() {
          {
            this.type = "USER";
            this.value = user_name;
          }
        };
        this.reservationEntries = clsClient.getGetFeaturesResponse().stream().map(feature -> new ReservationEntry() {
          {
            this.featureCount = String.valueOf(device_count);
            this.featureName = feature.featureName;
            this.featureVersion = feature.featureVersion;
          }
        }).toArray(ReservationEntry[]::new);
      }
    };

  
    String resgrpid = null;
    
    // get arm-poc
    final Optional<GetReservationGroupsResponse> resgrp = clsClient.getGetReservationGroupResponse().stream()
        .filter(grp -> grp.name.equals("arm-poc"))
        .findFirst();

    if (resgrp.isPresent()) {
      final GetReservationGroupsResponse group = resgrp.get();
      
      final Set<String> users = new HashSet<>();
      
      Trace.debug("get reservations");
      clsClient.getReservationGroupReservations(group.id);
      Trace.yaml(clsClient.getGetReservationsResponse().toArray());
      
      // get user names
      clsClient.getGetReservationsResponse().stream()
          .map(grp -> grp.hostId.value)
          .forEach(user -> users.add(user));

      // don't reserve if this user has a reservation
      if (users.contains(user_name)) {
        throw new RuntimeException("user '" + user_name + "' already has a reservation");
      }
      
      // check if there are enough features to reserve
      if (Integer.valueOf(features.get(0).featureCount) < device_count * (users.size() + 1)) {
        throw new RuntimeException("insufficient feature count to reserve another user");
      }
      
      Trace.debug("post reservation");
      clsClient.postReservation(group.id, reservation);
      Trace.yaml(clsClient.getPostReservationResponse());
      
      resgrpid = group.id;
    }
    else {
      Trace.debug("post reservation group");
      clsClient.postReservationGroup(new ReservationGroup() {
        {
          this.name = "arm-poc";
          this.reservations = new Reservation[] {
            reservation
          };
        }
      });
      Trace.yaml(clsClient.getPostReservationGroupResponse()); 
      
      // kludgy bit to get last element of url
      final String[] bits = clsClient.getPostReservationGroupResponse().uri.split("/");
      resgrpid = bits[bits.length - 1];
    };
    
    Trace.debug("get reservation group");
    clsClient.getReservationGroupReservations(resgrpid);
    Trace.yaml(clsClient.getGetReservationsResponse());
  }
  

  /**
   * @param host_id
   * @param host_name
   * @param user_name
   * @throws FlxException
   * @throws IOException
   */
  public void server(final HostIdType host_id_type, final String host_id, final String host_name, final String user_name) throws FlxException, IOException {
    final Client client = Client.create(
        Paths.get(config.rootPath, config.identityFilename),
        Paths.get(config.rootPath, config.storageLocation));
    
    /** use NULL if a FNE detected hostid is to be used otherwise, supply the STRING hostid */
    client.initialize(host_id_type, host_id);
    
    final Map<HostIdType, List<String>> hostIds = client.getAvailableHostids();
    Trace.yaml(hostIds);
    
    /** have we configured the hostid type etc. ? */
    if (!client.isPrivateDataConfigured()) {

      if (host_id_type == HostIdType.STRING) {
        client.configure(user_name, DeviceType.CLIENT, "jools test device");
      }
      else {
        client.configure(host_id_type, host_id, user_name, DeviceType.CLIENT, "jools test device");
      }
    }
    
    final String url = config.fnoHost + "/instances/" + config.clsId + "/request";
    final ICapabilityResponseData previewResponse = client.preview(url);
    Trace.debug("Preview response");
    Trace.yaml(previewResponse);
    
    final List<DesiredFeature> desiredFeatures = previewResponse.getFeatures().stream().map(f -> new DesiredFeature() {
      {
        this.name = f.getName();
        this.version = f.getVersion();
        this.count = 1;
      }
    }).collect(Collectors.toCollection(ArrayList::new));
    
    @SuppressWarnings("unused")
    final ICapabilityResponseData borrowResponse = client.borrow(url, desiredFeatures);
    
    final List<IFeature> features = client.getFeaturesInTrustedStorage();
    Trace.debug("Trusted storage");
    Trace.yaml(features);
    
    for (final DesiredFeature feature : desiredFeatures) {
      
      final ILicense license = client.acquire(feature);
      Trace.yaml(license);
    }
    
    client.releaseAll();
  }
  
}
//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
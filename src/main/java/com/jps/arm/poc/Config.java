//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc;

class Config {
  public String fnoHost;
  public String clsId;
  public String clsAdminUsername;
  public String clsAdminPassword;
  public String rootPath;
  public String storageLocation;
  public String identityFilename;
  public String responseFilename;
}
package com.jps.arm.poc.fne;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.flexnet.licensing.client.ICapabilityRequestOptions;
import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.IPrivateDataSource;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.HostIdType;
import com.flexnet.lm.SharedConstants.RequestOperation;
import com.flexnet.lm.net.Comm;
import com.jps.utils.Tracer;

public class Client {
  private final static Tracer Trace = Tracer.create(Client.class);

  private final static ObjectMapper json = new ObjectMapper(new JsonFactory())
      .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false)
      .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

  public enum DeviceType {
    CLIENT("FLX_CLIENT"), SERVER("FLX_SERVER");

    final String value;

    DeviceType(final String value) {
      this.value = value;
    }
  }

  protected byte[] identity;

  protected String storagePath;

  static class PrivateData {
    public HostIdType hostIdType;
    public String hostId;
    public String hostType;
    public String hostName;
    public String userName;
  }

  private PrivateData privateData;

  private ILicensing licensing;
  private ILicenseManager licenseManager;
  private IPrivateDataSource privateDatasource;

  private Client() {
  }

  public static Client create(final Path identity_path, final Path storage_path) {
    return new Client() {
      {
        try {

          this.identity = Files.readAllBytes(identity_path.toAbsolutePath());

          Files.newDirectoryStream(storage_path);

          this.storagePath = storage_path.toAbsolutePath().toString();
        }
        catch (final IOException e) {
          throw new RuntimeException(e);
        }
      }
    };
  }

  private void start() throws FlxException {
    Trace.in();
    try {
      Trace.info("configuring from private data store");

      // if a proper host id has been set then use it
      if (this.privateData.hostIdType != HostIdType.STRING) {
        this.licenseManager.setHostId(this.privateData.hostIdType, this.privateData.hostId);
      }
      
      this.licenseManager.setHostName(this.privateData.hostName);
      this.licenseManager.setHostType(this.privateData.hostType);

      Trace.yaml(this.privateData);
    }
    finally {
      Trace.out();
    }
  }

  public void initialize(final HostIdType host_id_type, final String host_id) throws FlxException {
    Trace.in();
    try {
      Trace.info("initializing licensing");

      this.licensing = host_id_type == HostIdType.STRING ? 
          LicensingFactory.getLicensing(this.identity, this.storagePath.toString(), host_id) :
          LicensingFactory.getLicensing(this.identity, this.storagePath.toString());

      this.licenseManager = this.licensing.getLicenseManager();

      this.privateDatasource = this.licensing.getPrivateDataSource();

      if (this.privateDatasource.exists(0)) {
        this.privateData = json.readValue(this.licensing.getPrivateDataSource().get(0), PrivateData.class);
        start();
      }
      else {
        Trace.info("private datasource is not configured");
      }
    }
    catch (final IOException e) {
      throw new RuntimeException(e);
    }
    finally {
      Trace.out();
    }
  }

  public boolean isPrivateDataConfigured() {
    return Objects.nonNull(this.privateData);
  }

  /**
   * configure assuming string hostid
   * @param user_name
   * @param host_type
   * @param host_name
   * @throws FlxException
   */
  public void configure(final String user_name, final DeviceType host_type, final String host_name) throws FlxException {
    Trace.in();
    try {
      this.privateData = new PrivateData() {
        {
          this.hostIdType = HostIdType.STRING;
          this.hostName = host_name;
          this.hostType = host_type.value;
          this.userName = user_name;
        }
      };

      this.privateDatasource.add(0, json.writeValueAsBytes(this.privateData));

      start();
    }
    catch (final JsonProcessingException e) {
      throw new RuntimeException(e);
    }
    finally {
      Trace.out();
    }
  }

  /**
   * configure with specific host id type and value
   * @param host_id_type
   * @param host_id
   * @param user_name
   * @param host_type
   * @param host_name
   * @throws FlxException
   */
  public void configure(final HostIdType host_id_type, final String host_id, final String user_name, final DeviceType host_type, final String host_name) throws FlxException {
    Trace.in();
    try {
      this.privateData = new PrivateData() {
        {
          this.hostIdType = host_id_type;
          this.hostId = host_id;
          this.hostName = host_name;
          this.hostType = host_type.value;
          this.userName = user_name;
        }
      };

      this.privateDatasource.add(0, json.writeValueAsBytes(this.privateData));

      start();
    }
    catch (final JsonProcessingException e) {
      throw new RuntimeException(e);
    }
    finally {
      Trace.out();
    }
  }
  
  public void terminate() {
    Trace.in();
    try {
      this.licensing.close();
    }
    finally {
      Trace.out();
    }
  }

  public Map<HostIdType, List<String>> getAvailableHostids() throws FlxException {
    Trace.in();
    return this.licenseManager.getHostIds();
  }

  public List<IFeature> getFeaturesInTrustedStorage() throws FlxException {
    Trace.in();
    return this.licenseManager.getFeaturesFromTrustedStorage(true);
  }

  public void setAvailableHostId (final HostIdType host_id_type, final String host_id) throws FlxException {
    this.licenseManager.setHostId(host_id_type, host_id);
  }
  
  public ICapabilityResponseData activate(final String url, final Collection<Rights> rights) throws FlxException, IOException {
    Trace.in();
    try {
      Trace.debug("creating cpability request options");
      final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions();

      options.setIncremental(false);

      rights.forEach(right -> {
        options.addRightsId(right.activationId, right.count);
      });

      Trace.debug("generate binary response");
      final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
      Files.write(Paths.get("request.bin"), requestData);

      Trace.debug("communicate with server - " + url);
      final Comm conn = Comm.getHttpInstance(url);

      final byte[] responseData = conn.sendBinaryMessage(requestData);
      Files.write(Paths.get("response.bin"), responseData);

      Trace.debug("process response");
      return this.licenseManager.processCapabilityResponse(responseData);
    }
    finally {
      Trace.out();
    }
  }

  public ICapabilityResponseData borrow(final String url, final Collection<DesiredFeature> desired_features) throws FlxException, IOException {
    Trace.in();
    try {
      Trace.debug("creating cpability request options");
      final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions();

      options.setIncremental(false);

      desired_features.forEach(feature -> {
        options.addDesiredFeature(feature.name, feature.version, feature.count);
      });
      options.addAuxiliaryHostId(HostIdType.USER, this.privateData.userName);
      
      Trace.debug("generate binary response");
      final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
      Files.write(Paths.get("request.bin"), requestData);

      Trace.debug("communicate with server - " + url);
      final Comm conn = Comm.getHttpInstance(url);

      final byte[] responseData = conn.sendBinaryMessage(requestData);
      Files.write(Paths.get("response.bin"), responseData);

      Trace.debug("process response");
      return this.licenseManager.processCapabilityResponse(responseData);
    }
    finally {
      Trace.out();
    }
  }
  
  @SuppressWarnings("serial")
  public void activate(final String url, final String aid) throws FlxException, IOException {
    activate(url, new ArrayList<Rights>() {
      {
        add(new Rights() {
          {
            this.activationId = aid;
            this.count = 1;
          }
        });
      }
    });
  }

  public ICapabilityResponseData preview(final String url) throws FlxException, IOException {
    Trace.in();
    try {
      Trace.debug("creating cpability request options");

      final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions();

      options.setRequestOperation(RequestOperation.PREVIEW);
      options.requestAllFeatures(true);
      // this gives the named user functionality
      options.addAuxiliaryHostId(HostIdType.USER, this.privateData.userName);

      Trace.debug("generate binary response");
      final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
      Files.write(Paths.get("request.bin"), requestData);

      Trace.debug("communicate with server - " + url);
      final Comm conn = Comm.getHttpInstance(url);

      final byte[] responseData = conn.sendBinaryMessage(requestData);
      Files.write(Paths.get("response.bin"), responseData);

      Trace.debug("process response");

      return this.licenseManager.getResponseDetails(responseData);
    }
    finally {
      Trace.out();
    }
  }

  public ICapabilityResponseData borrowAll(final String url) throws FlxException, IOException {
    Trace.in();
    try {
      Trace.debug("creating cpability request options");

      final ICapabilityRequestOptions options = this.licenseManager.createCapabilityRequestOptions();
      options.setIncremental(false);
      options.setRequestOperation(RequestOperation.REQUEST);
      // options.requestAllFeatures(true);
      // this gives the named user functionality
      options.addAuxiliaryHostId(HostIdType.USER, this.privateData.userName);

      Trace.debug("generate binary response");
      final byte[] requestData = this.licenseManager.generateCapabilityRequest(options);
      Files.write(Paths.get("request.bin"), requestData);

      Trace.debug("communicate with server - " + url);
      final Comm conn = Comm.getHttpInstance(url);

      final byte[] responseData = conn.sendBinaryMessage(requestData);
      Files.write(Paths.get("response.bin"), responseData);

      Trace.debug("process response");
      return this.licenseManager.processCapabilityResponse(responseData);
    }
    finally {
      Trace.out();
    }
  }
  
  public ILicense acquire(final DesiredFeature feature) throws FlxException {

    Trace.debug("acquire " + feature.toString());

    return this.licenseManager.acquire(
        feature.name, 
        feature.version, 
        feature.count);
  }

  public void release(final ILicense license) throws FlxException {
    Trace.in();
    this.licenseManager.returnLicense(license);
  }

  public void releaseAll() throws FlxException {
    Trace.in();
    this.licenseManager.returnAllLicenses();
  }
}

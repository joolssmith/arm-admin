//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.fne;

import java.util.Objects;

import com.flexnet.licensing.client.ILicense;

public class DesiredFeature {
  public String name;
  public String version;
  public int count;
  
  @Override
  public String toString() {
    return "DesiredFeature [name=" + name + ", version=" + version + ", count=" + count + "]";
  }
  
  public boolean matches(ILicense license) {
    return Objects.nonNull(license) && 
        license.getName().equals(this.name) && 
        license.getVersion().equals(this.version) && 
        license.getCount() == this.count;
  }
}
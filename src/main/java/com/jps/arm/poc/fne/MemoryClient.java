//********************************************** 
// Copyright (c) 2020 Flexera Software LLC 
// All rights reserved.
//**********************************************
package com.jps.arm.poc.fne;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import com.flexnet.licensing.client.ICapabilityResponseData;
import com.flexnet.licensing.client.IFeature;
import com.flexnet.licensing.client.ILicense;
import com.flexnet.licensing.client.ILicenseManager;
import com.flexnet.licensing.client.ILicensing;
import com.flexnet.licensing.client.LicensingFactory;
import com.flexnet.lm.FlxException;
import com.flexnet.lm.SharedConstants.HostIdType;
import com.jps.arm.poc.fne.Client.DeviceType;
import com.jps.utils.Tracer;

public class MemoryClient {
  private final static Tracer Trace = Tracer.create(MemoryClient.class);
 
  private ILicensing licensing;
  private ILicenseManager licenseManager;

  
  public void initialize(final String identity_path, final String host_id, final DeviceType host_type, final String host_name) throws FlxException {
    Trace.in();
    try {
//      Trace.info("initializing licensing");
      
      this.licensing = LicensingFactory.getLicensing(Files.readAllBytes(Paths.get(identity_path)), null, null);
//      Trace.info("licensing# %X",  this.licensing.hashCode());
      this.licenseManager = this.licensing.getLicenseManager();
//      Trace.info("manager# %X",  this.licenseManager.hashCode());
      this.licenseManager.setHostType(host_type.value); 
      this.licenseManager.setHostName(host_name);
      this.licenseManager.setHostId(HostIdType.USER, "juliansmith");
    }
    catch (final IOException e) {
      throw new RuntimeException(e);
    }
    finally {
//      Trace.out();
    }
  }
  
  public ICapabilityResponseData activate(final String response_path) throws FlxException {
    Trace.in();
    try {

      return this.licenseManager.processCapabilityResponse(response_path);
    }
    finally {
//      Trace.out();
    }
  }


  public ILicense acquire(final DesiredFeature feature) throws FlxException {
    Trace.in();
    return this.licenseManager.acquire(feature.name, feature.version, feature.count);
  }

  public void releaseAll() throws FlxException {
    Trace.in();
    this.licenseManager.returnAllLicenses();
  }
  
  public List<IFeature> getFeaturesInTrustedStorage() throws FlxException {
    return this.licenseManager.getFeaturesFromTrustedStorage(true);
  }
}

package com.jps.arm.poc;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.yaml.snakeyaml.Yaml;

import com.flexnet.lm.SharedConstants.HostIdType;
import com.jps.utils.Tracer;


public class Main {
  final static Tracer Trace = Tracer.create(Main.class);
  
  public static void main(final String... args) {
    Trace.in();

    try {
      final Yaml yaml = new Yaml(); 
      
      final Program program = new Program() {
        {
          this.config = yaml.loadAs(
              Files.newInputStream(Paths.get("properties.yaml")), 
              Config.class);
        }
      };
      
      /** reservation stuff */
      //program.reservations("freblo01", "DS000-LC-40001", 3);
      
      /** fne preview and borrow */
      //program.server(HostIdType.STRING, "julsmi01", "jools test device", "erijoh01");
      program.server(HostIdType.USER, "juliansmith", "jools test device", "erijoh01");
      
      //program.memory("jenkins");
    }
    catch(final Throwable t) {
      Trace.exception(t);
    }
    finally {
      Trace.out();
    }
  }
}
